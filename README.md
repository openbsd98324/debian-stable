# debian-stable


The release of Debian called "stable" is always the official released version of Debian. Ordinary users should use this version. See also DebianStability.

Bullseye (11.1) is the current stable release of Debian, and has been since 2021-10-09.


https://cdimage.debian.org/mirror/cdimage/archive/11.0.0/amd64/iso-dvd/debian-11.0.0-amd64-DVD-1.iso


